defmodule Mix.Tasks.ErlangAppInUmbrella do
  use Mix.Task

  @shortdoc "Creates an erlang app inside an Elixir umbrella app"

  def mixfile_contents(app_name) do
    """
    defmodule #{Macro.camelize(app_name)}.Mixfile do
      use Mix.Project

      def project do
        [app: :#{app_name},
        version: "0.1.0",
        build_path: "../../_build",
        config_path: "../../config/config.exs",
        deps_path: "../../deps",
        lockfile: "../../mix.lock",
        elixir: "~> 1.3",
        build_embedded: Mix.env == :prod,
        start_permanent: Mix.env == :prod,
        deps: deps]
      end

      # Configuration for the OTP application
      #
      # Type "mix help compile.app" for more information
      def application do
        [applications: [:logger],
        mod: {:#{app_name}_app, []}]
      end

      # Dependencies can be Hex packages:
      #
      #   {:mydep, "~> 0.3.0"}
      #
      # Or git/path repositories:
      #
      #   {:mydep, git: "https://github.com/elixir-lang/mydep.git", tag: "0.1.0"}
      #
      # To depend on another app inside the umbrella:
      #
      #   {:myapp, in_umbrella: true}
      #
      # Type "mix help deps" for more examples and options
      defp deps do
        []
      end
    end
    """
  end

  def run([app_name]) do
    File.cd("apps")
    System.cmd("rebar3", ["new", "app", app_name])
    mixfile_path = Path.join([app_name, "mix.exs"])
    File.write!(mixfile_path, mixfile_contents(app_name))
  end
end
