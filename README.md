# ErlangAppInUmbrella

**TODO: Add description**

## Installation

  1. Add `erlang_app_in_umbrella` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:erlang_app_in_umbrella, git: "https://gitlab.com/gonz/erlang_app_in_umbrella"}]
    end
    ```

## Usage
Run `mix erlang_app_in_umbrella app_name` in your umbrella project's root folder. The task
will automatically run whatever needs to be run and generate the mix file so that mix will
see the app.
